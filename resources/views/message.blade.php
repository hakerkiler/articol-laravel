@if (Session::has('message_msg') && (Session::get('message_type')) != null)
<div class="alert alert-{!! Session::get('message_type') !!} alert-dismissable" style="margin-top: 15px;">
    @if (Session::has('message_closable') && Session::get('message_closable') == true)
        <div class="col-md-1 pull-right">
            <a type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></a>
        </div>
    @endif
    <h4>
        @if(Session::get('message_type') == 'error' || Session::get('message_type') == 'danger')
            <i class="fa fa-exclamation-triangle"></i>
        @elseif(Session::get('message_type') == 'warning')
            <i class="fa fa-warning"></i>
        @elseif(Session::get('message_type') == 'info')
            <i class="fa fa-info"></i>
        @elseif(Session::get('message_type') == 'success')
            <i class="fa fa-check"></i>
        @endif
            @if(Session::get('message_type') == 'success')
                Succes
            @elseif(Session::get('message_type') == 'danger')
                Let op
            @else
                {!! ucfirst(Session::get('message_type')) !!}
            @endif
    </h4>
    <p>
        {!! Session::get('message_msg') !!}
    </p>
</div>
<div class="clearfix"></div>
@endif
