@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Article List
                        <a href="{{ route('article.create') }}" class="btn btn-default btn-xs pull-right">New</a>
                    </div>

                    <div class="panel-body">
                        @include('message')
                        <table class="table tab-content">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Title</th>
                                    <th>Content Short</th>
                                    <th>Author</th>
                                    <th>Create</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($articles as $article)
                                <tr>
                                    <td>{{ $article->id }}</td>
                                    <td>{{ $article->title }}</td>
                                    <td>{{ substr($article->content, 1, 30) }}</td>
                                    <td>{{ $article->author->name }}</td>
                                    <td>{{ $article->created_at }}</td>
                                    <td>
                                        {!! Form::open(['route' => ['article.destroy', $article->id], 'method' => 'delete', 'class' => 'form-horizontal']) !!}
                                        <div class="btn-group-sm">
                                            <a href="{{ route('article.show',$article->id) }}" class="btn btn-primary">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                            <a href="{{ route('article.edit',  $article->id) }}" class="btn btn-info" >
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                            <a href="javascript:;" onclick="$(this).closest('form').submit()"  class="btn btn-danger">
                                                <i class="fa fa-times"></i>
                                            </a>
                                        </div>
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                                @empty
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection