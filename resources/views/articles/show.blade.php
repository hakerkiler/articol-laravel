@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading text-center">
                        {{ $article->title }}
                    </div>

                    <div class="panel-body">
                        {{ $article->content }}
                    </div>
                    <div class="panel-footer">
                        Author: {{ $article->author->name }}
                        <span class="pull-right">
                            Created: {{ $article->created_at }}
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection