@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Add Form Article
                    </div>

                    <div class="panel-body">
                        {!! Form::open(['route' => ['article.store'], 'method' => 'post', 'class' => 'form-horizontal']) !!}

                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Title</label>

                            <div class="col-md-6">
                                {!! Form::text('title', old('title'), array('class' => 'form-control')) !!}

                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('content_full') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Full Text</label>

                            <div class="col-md-6">
                                {!! Form::textarea('content_full', old('content_full'), array('class' => 'form-control')) !!}

                                @if ($errors->has('content_full'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('content_full') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('author_id') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Author</label>

                            <div class="col-md-6">
                                {!! Form::select('author_id',['---'] + $authors->pluck('name', 'id')->toArray(), old('author_id'), array('class' => 'form-control')) !!}

                                @if ($errors->has('author_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('author_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-plus"></i> Add Article
                                </button>
                            </div>
                        </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection