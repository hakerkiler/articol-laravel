<?php

namespace App\Http\Controllers;

use App\Http\Requests\ArticleCreateRequest;
use App\Models\Article;
use App\Models\User;
use Illuminate\Http\Request;

use App\Http\Requests;

class ArticlesController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $articles = Article::all();
        return view('articles.index')
            ->with('articles', $articles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $authors = User::all();
        return view('articles.create')
            ->with('authors', $authors);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(ArticleCreateRequest $data)
    {
        $article = new Article();
        $article->title = $data->title;
        $article->content = $data->content_full;
        $article->user_id = $data->author_id;

        if($article->save()){
            $return_msg['message_msg'] = 'Article was save success.';
            $return_msg['message_type'] = 'success';
            $return_msg['message_closable'] = true;
        } else {
            $return_msg['message_msg'] = 'Save article give error.';
            $return_msg['message_type'] = 'warning';
            $return_msg['message_closable'] = false;
        }
        return redirect()->route('article.index')->with($return_msg);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $article = Article::find($id);
        return view('articles.show')
            ->with('article', $article);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $article = Article::find($id);
        $authors = User::all();
        return view('articles.edit')
            ->with('article', $article)
            ->with('authors', $authors);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(ArticleCreateRequest $data, $id)
    {
        $article = Article::find($id);
        if(!is_object($article)){
            $return_msg['message_msg'] = 'Article dont\'t find.';
            $return_msg['message_type'] = 'warning';
            $return_msg['message_closable'] = false;

            return redirect()->route('article.index')->with($return_msg);
        }

        $article->title = $data->title;
        $article->content = $data->content_full;
        $article->user_id = $data->author_id;

        $return_msg = array();

        if($article->save()){
            $return_msg['message_msg'] = 'Article was create success.';
            $return_msg['message_type'] = 'success';
            $return_msg['message_closable'] = true;
        } else {
            $return_msg['message_msg'] = 'Create article give error.';
            $return_msg['message_type'] = 'warning';
            $return_msg['message_closable'] = false;
        }
        return redirect()->route('article.index')->with($return_msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $article = Article::find($id);
        $return_msg = array();
        if(!is_object($article)){
            $return_msg['message_msg'] = 'Article dont\'t find.';
            $return_msg['message_type'] = 'warning';
            $return_msg['message_closable'] = false;

            return redirect()->route('article.index')->with($return_msg);
        }

        if($article->delete()){

            $return_msg['message_msg'] = 'Article is delete.';
            $return_msg['message_type'] = 'success';
            $return_msg['message_closable'] = false;
        }
        return redirect()->route('article.index')->with($return_msg);
    }

}
